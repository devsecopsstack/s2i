## [1.1.1](https://gitlab.com/to-be-continuous/s2i/compare/1.1.0...1.1.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([d2985f6](https://gitlab.com/to-be-continuous/s2i/commit/d2985f6b3fc2bfeb8a949f2613a389901049fd51))

# [1.1.0](https://gitlab.com/to-be-continuous/s2i/compare/1.0.1...1.1.0) (2024-1-27)


### Features

* migrate to CI/CD component ([d4b2552](https://gitlab.com/to-be-continuous/s2i/commit/d4b25520059bc8dc16f2bed243b54e772eb5ce94))

## [1.0.1](https://gitlab.com/to-be-continuous/s2i/compare/1.0.0...1.0.1) (2024-1-10)


### Bug Fixes

* fix issue when version for s2i is mentionned ([35f08b9](https://gitlab.com/to-be-continuous/s2i/commit/35f08b9f11feab187b0a7b6fb1a098392d07162f))

# 1.0.0 (2023-12-22)


### Features

* initial version ([66de639](https://gitlab.com/to-be-continuous/s2i/commit/66de639545a4eb59f62f437ea475e748af846051))
